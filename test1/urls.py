from django.conf.urls import url, include
from . import views
from django.views.generic import TemplateView

urlpatterns = [
  url('^users/create/$', views.create, name='user_create'),
  url('^users/(?P<id>[0-9]+)/$', views.view, name='user_view'),
  url('^users/(?P<id>[0-9]+)/edit/$', views.edit, name='user_edit'),
  url('', views.index, name='index'),
]
