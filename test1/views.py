from django.shortcuts import render
from django import http

from .services import (
  get_all_users,
  create_user,
  get_user,
)
from .forms import UserForm

def index(request):
  context = {
    'users': get_all_users()
  }
  return render(request, 'index.html', context)

def create(request):
  if request.method == 'POST':
    uf = UserForm(request.POST)
    if uf.is_valid():
      create_user(uf.cleaned_data)
      return http.HttpResponseRedirect('/')
    else:
      context = {
        'form': uf,
      }
      return render(request, 'create.html', context)
  else:
    context = {
      'form': UserForm(),
    }
    return render(request, 'create.html', context)

def view(request, id):
  user = get_user(id)
  context = {
    'user': user,
  }
  return render(request, 'view.html', context)

def edit(request, id):
  user = get_user(id)
  uf = UserForm(request.POST or None, instance=user)
  context = {
    'form': uf,
    'user': user,
  }
  if request.method == 'POST' and uf.is_valid():
    uf.save()  
    context['message'] = 'Successfully saved';
  return render(request, 'edit.html', context)
