from django.shortcuts import get_object_or_404
from .models import CustomUser
from datetime import date

def get_all_users():
  users = CustomUser.objects.all()
  return users

def get_user(id):
  user = get_object_or_404(CustomUser, pk=id)
  return user

def create_user(data):
  return CustomUser.objects.create_user(
    username=data['username'],
    password=data['password'],
    email=data['email'],
    birthday=data['birthday']
  )

def calculate_age(birthday):
  today = date.today()
  return today.year - birthday.year - ((today.month, today.day) < (birthday.month, birthday.day))
