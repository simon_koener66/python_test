from django import template
from ..services import calculate_age

register = template.Library()

@register.filter(name='addclass')
def addclass(value, arg):
  return value.as_widget(attrs={'class': arg})

@register.filter(name='eligibility')
def eligibility(value):
  if calculate_age(value) > 13:
    return 'allowed'
  else:
    return 'blocked'

@register.filter(name='bizzfuzz')
def bizzfuzz(value):
  result = ''
  if value % 3 == 0:
    result += 'Bizz'
  if value % 5 == 0:
    result += 'Fuzz'
  if not result:
    return value
  return result
