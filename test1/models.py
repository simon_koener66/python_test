from __future__ import unicode_literals
import random

from django.db import models
from django.contrib.auth.models import AbstractUser

def random_value():
  return random.randint(1, 100)

class CustomUser(AbstractUser):
  birthday = models.DateField()
  random_number = models.IntegerField(default=random_value)
