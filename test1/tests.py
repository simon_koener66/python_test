import datetime
from django.test import TestCase

from .models import CustomUser
from .services import (
  get_all_users,
  get_user,
  create_user,
)

class ServiceTest(TestCase):
  
  def test_add_user(self):
    data = {
      'username': 'test1',
      'email': 'test1@test.com',
      'birthday': '2003-5-5',
      'password': 'test',
      'confirm_password': 'test',
    }
    create_user(data)
    self.assertIsNot(CustomUser.objects.filter(username='test1').first(), None)

  def test_get_user(self):
    data = {
      'username': 'test2',
      'email': 'test1@test.com',
      'birthday': '2003-5-5',
      'password': 'test',
      'confirm_password': 'test1',
    }
    newuser = create_user(data)
    self.assertIsNot(get_user(newuser.id), None)
