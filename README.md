# Python Test Code

To get started:

- Activate virtual environment

- Go to Django project folder, and run `pip install -r requirements.txt` to install dependencies including Django 1.9

- Run `python manage.py migrate` to migrate database

- Start project by `python manage.py runserver`

## Release note

Finished tasks:

- User model extending with birthday and random_number fields
- User list, add, view, edit
- Unit tests for create_user() and get_user() functions

Not finished tasks:

- Excel file export

## More

Used Bootstrap 3.3.6 for styling
